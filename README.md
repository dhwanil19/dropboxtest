# DropboxTest

Dropbox Functional Testing using Selenium WebDriver(GUI) and Dropbox API V2(API)

Test Scenarios covered - 

Total 5 scenarios have been covered, 3 mandatory and 2 additional. Attaching the testbook for detailed mapping.


a) GUI


1) Login/Logout


2) Create New Folder


3) Upload File


4) Delete File


b) API


1) Search File

# Requirements:


1)Eclipse IDE with Maven installed.


2)Browser - Google Chrome


3)TestNG Plugin installed within Eclipse. Refer http://toolsqa.com/selenium-webdriver/install-testng/ if not installed.

Importing this project into Eclipse :
In eclipse go to File->Import->Maven-> Maven project with existing sources. In next section in root directory navigate to the project location , in project section below select the pom.xml file and click on finish.
Note : After configuration , if you get any errors in pom.xml file update the maven project configuration. Right click on project maven->update project configurations. Select force update snapshots/releases and click on Ok.

# Dropbox Configuration :

1)Make sure you have a dropbox account along with an app. If your account doesn't have an app you can login to you dropbox account and register for an app here https://www.dropbox.com/developers/apps.

2)After an app has been created , we need a unique token to access your dropbox account via selenium test cases. We can retrieve that by going in your created app and clicking on generate button under Generate access token section.

3)The access token will be used for searching file using Dropbox API v2(DBX Platform)

# TestNG.xml Setup
TestNG.xml in root folder takes parameters : email , password,a file present in dropbox and accessToken generated during configuration. Add it as follows -

1)Valid email and Valid password 
	<parameter name="valid_email" value='valid email id goes here' />
	<parameter name="valid_password" value='valid goes here' />
	
2)Access token for Dropbox
	<parameter name="accessToken" value='placeYourTokenHere' />
	Example Token - <parameter name="accessToken" value='JASCDSREQQQWEKQADSFADFAJJQREQW2ASDKSAFADSNFAJKJNJ' />	
	
3)File for search using API. This file should be present in your Dropbox root folder
   <parameter name="searchFileName" value='your filename.extension' />
   Example File Name- <parameter name="searchFileName" value="pom.xml" />

Without adding these parameters , the test case will not be able to run. Make sure to add correct details.

# Running Tests

After all the configurations right click on testng.xml 
Run As -> TestNG Suite.

# Reporting:

Test Evidence for each run can be found in the folder 'Test Evidence'.
It contains folder for each run in the format -'Run_dd-MMM-yyyy hh mm ss' e.g - Run_20-Sep-2018 09 46 00
Each run folder will have 3 test artifacts folders- 
1)'Screenshots' folder - contains '.jpg' screenshots taken during the run.
2)'ExtentReports' folder - contains detailed html report for entire run.
3)'FileUpload' folder - contains a text file created at runtime for uploading to Dropbox.