package com.dropbox.base;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.dropbox.common.TestUtil;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class BaseClass {

	public static WebDriver driver;
	public static ExtentReports report;
	public static Properties prop;
	public static ExtentTest results;
	public static WebDriverWait wait;
	public static String runFolderName;
	public static String screenShotFolder;
	public static String absolutePathFileUpload;

	@BeforeSuite
	public static void folderSetup() {
		// Create folder with runTimeStamp for reports
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy hh mm ss");
		runFolderName = "Run_" + dateFormat.format(now);

		String extentReportFolder = System.getProperty("user.dir") + "\\TestEvidence\\" + runFolderName
				+ "\\ExtentReports";
		File rptdir = new File(extentReportFolder);
		rptdir.mkdir();

		screenShotFolder = System.getProperty("user.dir") + "\\TestEvidence\\" + runFolderName + "\\Screenshots";
		File sc = new File(screenShotFolder);
		sc.mkdir();

		report = new ExtentReports(extentReportFolder + "\\TestReport.html", true);

		// Create txt file for upload
		String text = "Upload file for" + runFolderName;
		BufferedWriter output = null;
		String fileUploadFolder = System.getProperty("user.dir") + "\\TestEvidence\\" + runFolderName + "\\FileUpload";
		File upf = new File(fileUploadFolder);
		upf.mkdir();

		try {
			String txtFile = "UploadFile" + runFolderName + ".txt";
			File file = new File(txtFile);
			output = new BufferedWriter(new FileWriter(fileUploadFolder + "\\" + file));
			output.write(text);
			output.close();
			absolutePathFileUpload = fileUploadFolder + "\\" + txtFile;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void browserInitialization() {

		String browserName = prop.getProperty("browser").toLowerCase().trim();
		if (browserName.equals("chrome")) {
			// Set Google Chrome as Browser
			System.setProperty("webdriver.chrome.driver",
					System.getProperty("user.dir") + "\\drivers\\chromedriver.exe");
			driver = new ChromeDriver();
		} else if (browserName.equals("firefox")) {
			// Set FireFox as Browser
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\drivers\\geckodriver.exe");
			driver = new FirefoxDriver();
		} else if (browserName.equals("ie")) {
			// Set IE as Browser
			System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + "\\drivers\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		} else {
			System.out.println("Please provide correct browser type");
		}

		wait = new WebDriverWait(driver, TestUtil.IMPLICIT_TIMEOUT);
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_TIMEOUT, TimeUnit.SECONDS);
		driver.get(prop.getProperty("url"));

	}

	public BaseClass() {
		prop = new Properties();
		try {
			FileInputStream fs = new FileInputStream(System.getProperty("user.dir") + "\\config.properties");
			prop.load(fs);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void waitForElement(WebDriver driver, WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(element));
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getPageTitle() {
		TestUtil.waitForPageToLoad(driver);
		// wait.until(ExpectedConditions.titleContains("Home"));
		return driver.getTitle();
	}

	@AfterSuite
	public void tearDownSuite() {
		report.flush();
		report.close();

	}
}
