package com.dropbox.common;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;


public class TestUtil {
	public static long IMPLICIT_TIMEOUT=40;
	public static long PAGE_LOAD_TIMEOUT=40;



	public static String captureScreenshot(WebDriver driver,String folderPath, String screenShotName) throws IOException
	{
		TakesScreenshot ts = (TakesScreenshot)driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String dest = folderPath+"\\"+screenShotName+".jpeg";
		File destination = new File(dest);
		FileUtils.copyFile(source, destination);

		return dest;
	}

	public static void waitForPageToLoad(WebDriver driver) {

		driver.manage().timeouts().implicitlyWait(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
