package com.dropbox.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.dropbox.base.BaseClass;
import com.dropbox.common.TestUtil;

public class HomePage extends BaseClass {
	//Declare elements of HomePage according to Page Object Model conventions

	@FindBy(xpath = "//*[text()='Files']")
	WebElement btn_files;

	@FindBy(xpath = "//*[contains(text(), 'New folder')]")
	WebElement btn_newFolder;

	@FindBy(className = "notify-msg")
	WebElement notification_msg;

	@FindBy(xpath = "//input[@aria-label='Folder name']")
	WebElement txtbox_newFolderName;

	@FindBy(xpath = "//*[@alt='Account photo']")
	WebElement img_account;

	@FindBy(xpath = "//*[@href='https://www.dropbox.com/logout']")
	WebElement btn_signOut;

	@FindBy(xpath = "//button//span[contains(text(),'Upload')]")
	WebElement btn_upload;

	@FindBy(xpath = "//button[contains(text(),'Files')]")
	WebElement menu_files;

	@FindBy(xpath = "//*[contains(text(),'Uploaded')]")
	WebElement bar_notif;

	@FindBy(xpath="//div[contains(text(),'Delete')]")
	WebElement btn_delete;

	@FindBy(xpath="//span[contains(text(),'Delete')]")
	WebElement popup_delete;

	public String createNewFolder() {
		TestUtil.waitForPageToLoad(driver);
		wait.until(ExpectedConditions.visibilityOf(btn_newFolder));
		btn_newFolder.click();
		wait.until(ExpectedConditions.visibilityOf(txtbox_newFolderName));
		txtbox_newFolderName.sendKeys(runFolderName);
		txtbox_newFolderName.sendKeys("\n");
		wait.until(ExpectedConditions.visibilityOf(notification_msg));
		TestUtil.waitForPageToLoad(driver);

		return notification_msg.getText();
	}

	public String getHomePageTitle() {
		waitForElement(driver, img_account);
		return driver.getTitle();
	}

	public void openFiles() {
		TestUtil.waitForPageToLoad(driver);
		wait.until(ExpectedConditions.visibilityOf(btn_files));
		btn_files.click();
	}

	public void signOut() {
		TestUtil.waitForPageToLoad(driver);
		wait.until(ExpectedConditions.visibilityOf(img_account));
		img_account.click();
		wait.until(ExpectedConditions.visibilityOf(btn_signOut));
		btn_signOut.click();
		TestUtil.waitForPageToLoad(driver);

	}

	public void openUploadFilesWindow() {
		TestUtil.waitForPageToLoad(driver);

		wait.until(ExpectedConditions.visibilityOf(btn_upload));
		btn_upload.click();

		wait.until(ExpectedConditions.visibilityOf(menu_files));
		menu_files.click();

	}

	public String UploadFileUsingRobot(String imagepath) {
		TestUtil.waitForPageToLoad(driver);
		StringSelection stringSelection = new StringSelection(imagepath);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(stringSelection, null);

		Robot robot = null;

		try {
			robot = new Robot();
		} catch (AWTException e) {
			e.printStackTrace();
		}

		robot.delay(250);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.delay(150);
		robot.keyRelease(KeyEvent.VK_ENTER);
		TestUtil.waitForPageToLoad(driver);

		return bar_notif.getText();
	}

	public String deleteFile() {
		TestUtil.waitForPageToLoad(driver);
		wait.until(ExpectedConditions.visibilityOf(btn_delete));
		btn_delete.click();

		wait.until(ExpectedConditions.visibilityOf(popup_delete));
		popup_delete.click();
		TestUtil.waitForPageToLoad(driver);

		return notification_msg.getText();	}


}
