package com.dropbox.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.dropbox.base.BaseClass;
import com.dropbox.common.TestUtil;

public class LoginPage extends BaseClass {


	//Declare elements of LoginPage according to Page Object Model conventions
	@FindBy(xpath = "//*[@class='login-button signin-button button-primary']")
	WebElement btn_login;

	@FindBy(xpath = "//span[@class='error-message']")
	WebElement error_msg;

	@FindBy(name = "login_email")
	WebElement txtBox_email;

	@FindBy(name = "login_password")
	WebElement txtBox_password;

	@FindBy(xpath = "//*[contains(text(), 'Select all images with')]")
	List<WebElement> img_captcha;

	public LoginPage() {
		PageFactory.initElements(driver, this);

	}

	//Method to perform Login activity for valid credentials
	public HomePage validLogin(String email, String password) {
		TestUtil.waitForPageToLoad(driver);
		wait.until(ExpectedConditions.visibilityOf(txtBox_email));
		txtBox_email.sendKeys(email);
		wait.until(ExpectedConditions.visibilityOf(txtBox_password));
		txtBox_password.sendKeys(password);
		wait.until(ExpectedConditions.visibilityOf(btn_login));
		btn_login.click();
		TestUtil.waitForPageToLoad(driver);
		return new HomePage();

	}

	//Method to perform Login activity for invalid credentials
	public String invalidLogin(String email, String password) {
		TestUtil.waitForPageToLoad(driver);
		wait.until(ExpectedConditions.visibilityOf(txtBox_email));
		txtBox_email.sendKeys(email);
		wait.until(ExpectedConditions.visibilityOf(txtBox_password));
		txtBox_password.sendKeys(password);
		wait.until(ExpectedConditions.visibilityOf(btn_login));
		btn_login.click();
		TestUtil.waitForPageToLoad(driver);
		return driver.getTitle();
	}

}
