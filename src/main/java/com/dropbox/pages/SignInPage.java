package com.dropbox.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.dropbox.base.BaseClass;

public class SignInPage extends BaseClass {

	//Declare elements of SignInPage according to Page Object Model conventions

	@FindBy(xpath = "//button[contains(text(), 'Sign in')]")
	WebElement btn_SignIn;

	public SignInPage() {
		PageFactory.initElements(driver, this);
	}

	public LoginPage SignIn() {
		Actions action = new Actions(driver);
		action.moveToElement(btn_SignIn).click().perform();

		return new LoginPage();

	}
}
