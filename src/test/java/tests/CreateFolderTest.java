package tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.dropbox.base.BaseClass;
import com.dropbox.common.TestUtil;
import com.dropbox.pages.HomePage;
import com.dropbox.pages.LoginPage;
import com.dropbox.pages.SignInPage;
import com.relevantcodes.extentreports.LogStatus;

public class CreateFolderTest extends BaseClass{
	//In next test
	HomePage homePage;
	LoginPage loginPage;
	SignInPage signInPage;

	public CreateFolderTest() {
		super();
	}

	@BeforeMethod
	public void setUp() {
		browserInitialization();
		signInPage = new SignInPage();
	}



	@Test(priority = 2)
	@Parameters({"valid_email","valid_password"})
	public void createNewFolder(String valid_email , String valid_password) throws Exception {
		results = report.startTest("Create New Folder", "Create New folder after user login");
		results.assignCategory("Create Folder Scenario");

		loginPage = signInPage.SignIn();
		homePage = loginPage.validLogin(valid_email,valid_password);
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.openFiles();
		String text = homePage.createNewFolder();

		if (text.contains("Created folder")) {
			String screenshot =TestUtil.captureScreenshot(driver, screenShotFolder, "Created New Folder");
			results.log(LogStatus.PASS, "Folder created successfully. Folder name is - " + text.replace("Created folder", "").trim() +"'\n"+"Screenshot - "
					+ results.addScreenCapture(screenshot));
		} else {
			String screenshot =TestUtil.captureScreenshot(driver, screenShotFolder, "Created New Folder - Error");
			results.log(LogStatus.FAIL, "Unable to create folder."+"'\n"+"Screenshot - "
					+ results.addScreenCapture(screenshot));

		}
	}

	@AfterMethod
	public void closeBrowser() {
		report.endTest(results);
		driver.quit();
	}

}
