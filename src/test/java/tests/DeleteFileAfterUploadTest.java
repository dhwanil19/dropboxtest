package tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.dropbox.base.BaseClass;
import com.dropbox.common.TestUtil;
import com.dropbox.pages.HomePage;
import com.dropbox.pages.LoginPage;
import com.dropbox.pages.SignInPage;
import com.relevantcodes.extentreports.LogStatus;

public class DeleteFileAfterUploadTest extends BaseClass {
	HomePage homePage;
	LoginPage loginPage;
	SignInPage signInPage;

	public DeleteFileAfterUploadTest() {
		super();
	}

	@BeforeMethod
	public void setUp() {
		browserInitialization();
		signInPage = new SignInPage();
	}

	@Test(priority=3)
	@Parameters({ "valid_email", "valid_password" })
	public void deleteFileAfterUpload(String valid_email, String valid_password) throws Exception {
		results = report.startTest("Delete Uploaded File from Dropbox", "Validate successful file deletion from Dropbox");
		results.assignCategory("Delete File Scenario");

		loginPage = signInPage.SignIn();
		homePage = loginPage.validLogin(valid_email,valid_password);
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.openFiles();

		homePage.openUploadFilesWindow();
		String text = homePage.UploadFileUsingRobot(absolutePathFileUpload);
		if(text.contains("Uploaded")) {
			String screenshot =TestUtil.captureScreenshot(driver, screenShotFolder, "FileUpload");
			results.log(LogStatus.PASS, "Successfully uploaded file"+"\n"+"Screenshot - "
					+ results.addScreenCapture(screenshot));
		}else {
			String screenshot =TestUtil.captureScreenshot(driver, screenShotFolder, "FileUpload -Error");
			results.log(LogStatus.FAIL, "File upload failed."+"\n"+"Screenshot - "
					+ results.addScreenCapture(screenshot));
		}

		String notif_txt = homePage.deleteFile();

		if(notif_txt.contains("Deleted")) {
			String screenshot =TestUtil.captureScreenshot(driver, screenShotFolder, "DeleteFile");
			results.log(LogStatus.PASS, "Successfully deleted file"+"\n"+"Screenshot - "
					+ results.addScreenCapture(screenshot));
		}else {
			String screenshot =TestUtil.captureScreenshot(driver, screenShotFolder, "DeleteFile -Error");
			results.log(LogStatus.FAIL, "Unable to delete."+"\n"+"Screenshot - "
					+ results.addScreenCapture(screenshot));
		}

	}

	@AfterMethod
	public void closeBrowser() {
		report.endTest(results);
		driver.quit();
	}
}
