package tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.dropbox.base.BaseClass;
import com.dropbox.common.TestUtil;
import com.dropbox.pages.HomePage;
import com.dropbox.pages.LoginPage;
import com.dropbox.pages.SignInPage;
import com.relevantcodes.extentreports.LogStatus;

public class LoginTest extends BaseClass {

	HomePage homePage;
	LoginPage loginPage;
	SignInPage signInPage;

	public LoginTest() {
		super();
	}

	@BeforeMethod
	public void setUp() {
		browserInitialization();
		signInPage = new SignInPage();
	}

	@Test(priority = 0)
	@Parameters({ "valid_email", "valid_password" })
	public void loginValidCredentials(String valid_email, String valid_password) throws Exception {
		results = report.startTest("Login - Valid Credentials", "Validate successful Login for correct credentials");
		results.assignCategory("Login/Logout Scenario");

		loginPage = signInPage.SignIn();
		homePage = loginPage.validLogin(valid_email, valid_password);
		homePage = PageFactory.initElements(driver, HomePage.class);
		String title = homePage.getHomePageTitle();

		if (title.contains("Home")) {
			String screenshot = TestUtil.captureScreenshot(driver, screenShotFolder, "Valid Login");
			results.log(LogStatus.PASS, "Login successful.Login Page title is - " + title + "Screenshot - "
					+ results.addScreenCapture(screenshot));

		} else {
			String screenshot = TestUtil.captureScreenshot(driver, screenShotFolder, "Valid Login - Error");
			results.log(LogStatus.FAIL, "Login Failed.Login Page title is - " + title + title + "Screenshot - "
					+ results.addScreenCapture(screenshot));

		}
	}

	@Test(dependsOnMethods = { "loginValidCredentials" })
	@Parameters({ "invalid_email", "valid_password" })
	public void loginInvalidEmail(String invalid_email, String valid_password) throws Exception {
		results = report.startTest("Login - Invalid Credentials", "Validate failed Login attempt for incorrect Email");
		results.assignCategory("Login/Logout Scenario");

		loginPage = signInPage.SignIn();
		String title = loginPage.invalidLogin(invalid_email, valid_password);
		if (title.contains("Dropbox")) {
			String screenshot = TestUtil.captureScreenshot(driver, screenShotFolder, "Invalid Email");
			results.log(LogStatus.PASS, "Login failure as expected. Incorrect email" + title + "Screenshot - "
					+ results.addScreenCapture(screenshot));
		} else {
			String screenshot = TestUtil.captureScreenshot(driver, screenShotFolder, "Invalid Email-Error");
			results.log(LogStatus.FAIL, "No Login failure for incorrect email. Page title is - " + title
					+ "Screenshot - " + results.addScreenCapture(screenshot));

		}
	}

	@Test(dependsOnMethods = { "loginInvalidEmail" })
	@Parameters({ "valid_email", "invalid_password" })
	public void loginInvalidPassword(String valid_email, String invalid_password) throws Exception {
		results = report.startTest("Login - Invalid Credentials",
				"Validate failed Login attempt for incorrect Password");
		results.assignCategory("Login/Logout Scenario");

		loginPage = signInPage.SignIn();
		String title = loginPage.invalidLogin(valid_email, invalid_password);
		if (title.contains("Dropbox")) {
			String screenshot = TestUtil.captureScreenshot(driver, screenShotFolder, "Invalid Password");
			results.log(LogStatus.PASS, "Login failure as expected. Incorrect password. Screenshot - "
					+ results.addScreenCapture(screenshot));
		} else {
			String screenshot = TestUtil.captureScreenshot(driver, screenShotFolder, "Invalid Password - Error");
			results.addScreenCapture(screenshot);
			results.log(LogStatus.FAIL, "No Login failure for incorrect password. Page title is - " + title
					+ "Screenshot - " + results.addScreenCapture(screenshot));

		}
	}

	@Test(dependsOnMethods = { "loginInvalidPassword" })
	@Parameters({ "valid_email", "valid_password" })
	public void loginAndLogout(String valid_email, String valid_password) throws Exception {
		results = report.startTest("Login and Logout - Valid Credentials",
				"Validate successful Logout for correct credentials");
		results.assignCategory("Login/Logout Scenario");

		loginPage = signInPage.SignIn();
		homePage = loginPage.validLogin(valid_email, valid_password);
		homePage = PageFactory.initElements(driver, HomePage.class);
		String title = homePage.getHomePageTitle();

		if (title.contains("Home")) {
			String screenshot = TestUtil.captureScreenshot(driver, screenShotFolder, "Valid Login");
			results.log(LogStatus.PASS, "Login successful.Login Page title is - " + title + "Screenshot - "
					+ results.addScreenCapture(screenshot));

		} else {
			String screenshot = TestUtil.captureScreenshot(driver, screenShotFolder, "Valid Login - Error");
			results.log(LogStatus.FAIL, "Login Failed.Login Page title is - " + title + "Screenshot - "
					+ results.addScreenCapture(screenshot));

		}

		homePage.signOut();
		title = getPageTitle();
		if (title.contains("Login")) {
			String screenshot = TestUtil.captureScreenshot(driver, screenShotFolder, "LogoutSuccessful");
			results.log(LogStatus.PASS,
					"Successfully logged out of Dropbox. Screenshot - " + results.addScreenCapture(screenshot));
		} else {
			String screenshot = TestUtil.captureScreenshot(driver, screenShotFolder, "LogoutSuccessful -Error");
			results.addScreenCapture(screenshot);
			results.log(LogStatus.FAIL, "Unable to log out of Dropbox . Page title is - " + title + "Screenshot - "
					+ results.addScreenCapture(screenshot));
		}
	}

	@AfterMethod
	public void tearDown() {
		report.endTest(results);
		driver.quit();
	}
}