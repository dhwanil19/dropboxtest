package tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.dropbox.base.BaseClass;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.Metadata;
import com.dropbox.core.v2.users.FullAccount;
import com.relevantcodes.extentreports.LogStatus;

public class SearchFilesAPITest extends BaseClass {
	DbxClientV2 client;

	public SearchFilesAPITest() {
		super();
	}

	@Test(priority = 0)
	@Parameters({ "accessToken" ,"searchFileName"})
	public void searchFilesAPITest(String accessToken,String searchFileName ) throws Exception {
		results = report.startTest("Search Files", "Search file from backend using Dropbox API");
		results.assignCategory("Search File(API) Scenario");
		String userAccount = dropBoxAuth(accessToken);
		if (!(userAccount.isEmpty())) {
			results.log(LogStatus.PASS, "User authentication via API successful. Account name - " + userAccount);
		} else {
			results.log(LogStatus.FAIL, "Failed User authentication via API.Please check accss token");
		}

		boolean isFilePresent=searchFileAPI(searchFileName);

		if (isFilePresent) {
			results.log(LogStatus.PASS, "File found in Dropbox. Filename - " + searchFileName);
		} else {
			results.log(LogStatus.FAIL, "File " + searchFileName + " not found in Dropbox");
		}
	}

	public String dropBoxAuth(String accessToken) throws DbxException {

		DbxRequestConfig config = DbxRequestConfig.newBuilder("dropbox/java-tutorial").build();
		client = new DbxClientV2(config, accessToken);
		FullAccount account = client.users().getCurrentAccount();
		return account.getName().getDisplayName();

	}

	public Boolean searchFileAPI(String fileName) throws Exception {

		Boolean filePresent = false;
		// Get files and folder metadata from Dropbox root directory
		ListFolderResult result = client.files().listFolder("");
		while (true) {
			for (Metadata metadata : result.getEntries()) {
				if (metadata.getPathDisplay().replace("/", "").equals(fileName)) {
					filePresent = true;
					break;
				}
			}

			if (!result.getHasMore()) {
				break;
			}

			result = client.files().listFolderContinue(result.getCursor());
		}
		return filePresent;
	}

	@AfterMethod
	public void tearDown() {
		report.endTest(results);
	}
}
