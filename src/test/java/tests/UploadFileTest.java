package tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.dropbox.base.BaseClass;
import com.dropbox.common.TestUtil;
import com.dropbox.pages.HomePage;
import com.dropbox.pages.LoginPage;
import com.dropbox.pages.SignInPage;
import com.relevantcodes.extentreports.LogStatus;

public class UploadFileTest extends BaseClass {

	HomePage homePage;
	LoginPage loginPage;
	SignInPage signInPage;

	public UploadFileTest() {
		super();
	}

	@BeforeMethod
	public void setUp() {
		browserInitialization();
		signInPage = new SignInPage();
	}

	@Test(priority=1)
	@Parameters({ "valid_email", "valid_password" })
	public void uploadFileToDropbox(String valid_email, String valid_password) throws Exception {
		results = report.startTest("Upload file to Dropbox", "Validate successful file upload to Dropbox");
		results.assignCategory("File Upload Scenario");

		loginPage = signInPage.SignIn();
		homePage = loginPage.validLogin(valid_email,valid_password);
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.openFiles();

		homePage.openUploadFilesWindow();
		String text = homePage.UploadFileUsingRobot(absolutePathFileUpload);
		if(text.contains("Uploaded")) {
			String screenshot =TestUtil.captureScreenshot(driver, screenShotFolder, "FileUpload");
			results.log(LogStatus.PASS, "Successfully uploaded file"+"\n"+"Screenshot - "
					+ results.addScreenCapture(screenshot));
		}else {
			String screenshot =TestUtil.captureScreenshot(driver, screenShotFolder, "FileUpload -Error");
			results.log(LogStatus.FAIL, "File upload failed."+"\n"+"Screenshot - "
					+ results.addScreenCapture(screenshot));
		}
	}

	@AfterMethod
	public void closeBrowser() {
		report.endTest(results);
		driver.quit();
	}
}
